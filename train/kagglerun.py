import itertools

import torch
import torch.optim as optim
from allennlp.data.dataset_readers.seq2seq import Seq2SeqDatasetReader
from allennlp.data.iterators import BucketIterator
from allennlp.data.token_indexers import SingleIdTokenIndexer
from allennlp.data.tokenizers.character_tokenizer import CharacterTokenizer
from allennlp.data.tokenizers.word_tokenizer import WordTokenizer
from allennlp.data.vocabulary import Vocabulary
from allennlp.nn.activations import Activation
from allennlp.models.encoder_decoders.simple_seq2seq import SimpleSeq2Seq
from allennlp.modules.attention import LinearAttention, BilinearAttention, DotProductAttention
from allennlp.modules.seq2seq_encoders import PytorchSeq2SeqWrapper, StackedSelfAttentionEncoder
from allennlp.modules.text_field_embedders import BasicTextFieldEmbedder
from allennlp.modules.token_embedders import Embedding
from allennlp.predictors import SimpleSeq2SeqPredictor
from allennlp.training.trainer import Trainer

EN_EMBEDDING_DIM = 256
ZH_EMBEDDING_DIM = 256
HIDDEN_DIM = 256
CUDA_DEVICE = 0

def main():
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    reader = Seq2SeqDatasetReader(
        lazy=True,
        # source_tokenizer=CharacterTokenizer(),
        # target_tokenizer=CharacterTokenizer(),
        source_tokenizer=WordTokenizer(),
        target_tokenizer=WordTokenizer(),
        source_token_indexers={'tokens': SingleIdTokenIndexer()},
        target_token_indexers={'tokens': SingleIdTokenIndexer(namespace='target_tokens')})
    # train_dataset = reader.read('../input/trainnobpe.tsv')
    # validation_dataset = reader.read('../input/devnobpe.tsv')
    # test_dataset = reader.read('../input/testnobpe.tsv')
    train_dataset = reader.read('../input/trainbpeall.tsv')
    validation_dataset = reader.read('../input/devbpeall.tsv')
    test_dataset = reader.read('../input/testbpeall.tsv')


    # vocab = Vocabulary.from_instances(train_dataset + validation_dataset,
    #                                   min_count={'tokens': 4, 'target_tokens': 4})

    vocab = Vocabulary.from_instances(train_dataset,min_count={'tokens': 1, 'target_tokens': 1})

    en_embedding = Embedding(num_embeddings=vocab.get_vocab_size('tokens'),
                             embedding_dim=EN_EMBEDDING_DIM)
    # encoder = PytorchSeq2SeqWrapper(
    #     torch.nn.LSTM(EN_EMBEDDING_DIM, HIDDEN_DIM, batch_first=True))
    encoder = StackedSelfAttentionEncoder(input_dim=EN_EMBEDDING_DIM, hidden_dim=HIDDEN_DIM, projection_dim=128, feedforward_hidden_dim=128, num_layers=1, num_attention_heads=8)

    source_embedder = BasicTextFieldEmbedder({"tokens": en_embedding})

    # attention = LinearAttention(HIDDEN_DIM, HIDDEN_DIM, activation=Activation.by_name('tanh')())
    # attention = BilinearAttention(HIDDEN_DIM, HIDDEN_DIM)
    attention = DotProductAttention()

    max_decoding_steps = 20   # TODO: make this variable was 20
    model = SimpleSeq2Seq(vocab, source_embedder, encoder, max_decoding_steps,
                          target_embedding_dim=ZH_EMBEDDING_DIM,
                          target_namespace='target_tokens',
                          attention=attention,
                          beam_size=8,
                          use_bleu=True)

    model = model.to(device)                     
    optimizer = optim.Adam(model.parameters())
    iterator = BucketIterator(batch_size=128, sorting_keys=[("source_tokens", "num_tokens")]) #was 8,64 batch_size

    iterator.index_with(vocab)

    trainer = Trainer(model=model,
                      optimizer=optimizer,
                      iterator=iterator,
                      train_dataset=train_dataset,
                      validation_dataset=validation_dataset,
                      num_epochs=1,
                      cuda_device=CUDA_DEVICE)

    for i in range(5): #was 5 5hh
        print('Epoch: {}'.format(i))
        trainer.train()

        predictor = SimpleSeq2SeqPredictor(model, reader)
        with open('output.txt', 'w') as file:
            for instance in itertools.islice(test_dataset, 40):
                for tok in instance.fields['source_tokens'].tokens:
                    file.write(str(tok)+' ')
                file.write('\n')
                # for tok in instance.fields['target_tokens'].tokens:
                #     file.write(str(tok)+' ')
                # file.write('\n')
                for tok in predictor.predict_instance(instance)['predicted_tokens']:
                    file.write(str(tok)+' ')
                file.write('\n\n')    

main()