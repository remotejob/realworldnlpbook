# realworldnlpbook
http://www.realworldnlpbook.com/blog/building-seq2seq-machine-translation-models-using-allennlp.html
https://github.com/mhagiwara/realworldnlp/blob/master/examples/mt/mt.py
https://madflex.de/posts/allennlp--machine-translation-using-configuration/

export KAGGLE_CONFIG_DIR=/exwindoz/home/juno/kaggleapi


python src/create_bitext.py eng_fin sentences.csv links.csv | cut -f3,6 > data/tatoeba.eng_fin.tsv

cat data/tatoeba.eng_fin.tsv | awk 'NR%10==1' > data/tatoeba.eng_fin.test.tsv
cat data/tatoeba.eng_fin.tsv | awk 'NR%10==2' > data/tatoeba.eng_fin.dev.tsv
cat data/tatoeba.eng_fin.tsv | awk 'NR%10!=1&&NR%10!=2' > data/tatoeba.eng_fin.train.tsv


kaggle datasets create  -p data/

kaggle datasets version -p data/ -m "Updated data 0"

kaggle kernels push -p train/
kaggle kernels status remotejob/realworldnlpbook

rm -rf output/*
kaggle kernels output remotejob/realworldnlpbook -p output/

09.08.2019

paste -d"\t" /exwindoz/home/juno/repos/gitlab.com/remotejob/joeynmtreverse/data/finbot/data/trainnobpe.ask /exwindoz/home/juno/repos/gitlab.com/remotejob/joeynmtreverse/data/finbot/data/trainnobpe.ans  > data/trainnobpe.tsv
paste -d"\t" /exwindoz/home/juno/repos/gitlab.com/remotejob/joeynmtreverse/data/finbot/data/devnobpe.ask /exwindoz/home/juno/repos/gitlab.com/remotejob/joeynmtreverse/data/finbot/data/devnobpe.ans  > data/devnobpe.tsv
paste -d"\t" /exwindoz/home/juno/repos/gitlab.com/remotejob/joeynmtreverse/data/finbot/data/testnobpe.ask /exwindoz/home/juno/repos/gitlab.com/remotejob/joeynmtreverse/data/finbot/data/testnobpe.ask  > data/testnobpe.tsv

#spm_train --input=data/trainnobpe.tsv,data/devnobpe.tsv,data/testnobpe.tsv --model_prefix=m --vocab_size=10000 --num_threads=32 --input_sentence_size=100000 --shuffle_input_sentence=true
cp /exwindoz/home/juno/repos/gitlab.com/remotejob/joeynmtreverse/m.* .

cut -f1 -d$'\t' /home/juno/repos/github.com/TurkuNLP/Turku-neural-parser-pipeline/seq2seqfinbottrain.tsv >/tmp/tmpnobpe.ask
spm_encode  --output_format=id --model=m.model < /tmp/tmpnobpe.ask  > /tmp/tmpbpe.ask
cut -f2 -d$'\t' /home/juno/repos/github.com/TurkuNLP/Turku-neural-parser-pipeline/seq2seqfinbottrain.tsv >/tmp/tmpnobpe.ans
spm_encode  --output_format=id --model=m.model < /tmp/tmpnobpe.ans  > /tmp/tmpbpe.ans
paste -d"\t" /tmp/tmpbpe.ask /tmp/tmpbpe.ans > data/trainbpe.tsv

cut -f1 -d$'\t'  /home/juno/repos/github.com/TurkuNLP/Turku-neural-parser-pipeline/seq2seqfinbotdev.tsv>/tmp/tmpnobpe.ask
spm_encode  --output_format=id --model=m.model < /tmp/tmpnobpe.ask  > /tmp/tmpbpe.ask
cut -f2 -d$'\t' /home/juno/repos/github.com/TurkuNLP/Turku-neural-parser-pipeline/seq2seqfinbotdev.tsv >/tmp/tmpnobpe.ans
spm_encode  --output_format=id --model=m.model < /tmp/tmpnobpe.ans  > /tmp/tmpbpe.ans
paste -d"\t" /tmp/tmpbpe.ask /tmp/tmpbpe.ans > data/devbpe.tsv

#cut -f1 -d$'\t' data/testnobpe.tsv >/tmp/tmpnobpe.ask
spm_encode  --output_format=id --model=m.model < /home/juno/gowork/src/gitlab.com/remotejob/mltwiiter/data/testnobpe.ask > /tmp/tmpbpe.ask
spm_encode  --output_format=id --model=m.model < /home/juno/gowork/src/gitlab.com/remotejob/mltwiiter/data/testnobpe.ask  > /tmp/tmpbpe.ans
paste -d"\t" /tmp/tmpbpe.ask /tmp/tmpbpe.ans > data/testbpe.tsv



#spm_encode  --output_format=id --model=m.model < data/trainnobpe.tsv  > data/trainbpe.tsv
#spm_encode  --output_format=id --model=m.model < data/devnobpe.tsv > data/devbpe.tsv 
#spm_encode  --output_format=id --model=m.model < data/testnobpe.tsv > data/testbpe.tsv



spm_decode --model=m.model --input_format=id  output/output.txt > output/test.txt


allennlp train finbot.json -f -s output


allennlp evaluate model.tar.gz data/testbpe.tsv

cat <<EOF > inputs.txt
{"source": "Let's try something."}
EOF


allennlp predict model.tar.gz inputs.txt --predictor seq2seq

15.08.2019
paste -d"\t" /exwindoz/home/juno/repos/gitlab.com/remotejob/joeynmtreverse/data/finbot/data/trainbpeall.ask /exwindoz/home/juno/repos/gitlab.com/remotejob/joeynmtreverse/data/finbot/data/trainbpeall.ans  > data/trainbpeall.tsv
paste -d"\t" /exwindoz/home/juno/repos/gitlab.com/remotejob/joeynmtreverse/data/finbot/data/devbpeall.ask /exwindoz/home/juno/repos/gitlab.com/remotejob/joeynmtreverse/data/finbot/data/devbpeall.ans  > data/devbpeall.tsv
paste -d"\t" /exwindoz/home/juno/repos/gitlab.com/remotejob/joeynmtreverse/data/finbot/data/testbpeall.ask /exwindoz/home/juno/repos/gitlab.com/remotejob/joeynmtreverse/data/finbot/data/testbpeall.ask  > data/testbpeall.tsv








